package com.collection.implementations;

import com.collection.interfaces.IList1;
import java.util.Arrays;
import java.util.Objects;

public class AList1 implements IList1 {
    private int capacity;
    private int[] array;
    private int size;

    public AList1() {
        this.capacity = 10;
        this.array = new int[this.capacity];
        this.size = 0;
    }

    public AList1(int capacity) {
        this.capacity = capacity;
        this.array = new int[this.capacity];
        this.size = 0;
    }

    public AList1(int[] array) {
        this();
        for (int i = 0; i < array.length; i++) {
            add(array[i]);
        }
    }

    @Override
    public void clear() {
        this.capacity = 10;
        array = new int[this.capacity];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        else {
            int value = array[index];
            return value;
        }
    }

    @Override
    public boolean add(int value) {
        return add(size, value);
    }

    @Override
    public boolean add(int index, int value) {
        if (index > size || index < 0) {
            return false;
        }
        else {
            if (size == capacity) {
                increaseCapacity();
            }

            int[] newArray = new int[capacity];
            int oldIndex = 0;
            for (int i = 0; i < newArray.length; i++) {
                if (index == i) {
                    newArray[i] = value;
                }
                else {
                    newArray[i] = array[oldIndex];
                    oldIndex++;
                }
            }

            array = newArray;
            size++;
            return true;
        }
    }

    @Override
    public int remove(int number) {
        int removedElement = -1;
        for (int i = 0; i < size; i++) {
            if (array[i] == number) {
                removedElement = removeByIndex(i);
                break;
            }
        }
        return removedElement;
    }

    @Override
    public int removeByIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        else {
            int removedElement = array[index];
            int[] newArray = new int[capacity];
            int newIndex = 0;
            for (int i = 0; i < array.length; i++) {
                if (i != index){
                    newArray[newIndex] = array[i];
                    newIndex++;
                }
            }
            array = newArray;
            size--;
            return removedElement;
        }
    }

    @Override
    public boolean contains(int value) {
        for (int i = 0; i < size; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean set(int index, int value) {
        if (index >= size || index < 0) {
            return false;
        }
        else {
            array[index] = value;
            return true;
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        for (int i = 0; i < size; i++) {
            stringArray += array[i];
            if (i != size - 1) {
                stringArray += ", ";
            }
        }
        stringArray += "]\n";
        System.out.println(stringArray);
    }

    @Override
    public int[] toArray() {
        return Arrays.copyOf(array, size);
    }

    @Override
    public boolean removeAll(int[] arr) {
        if (arr == null){
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            remove(arr[i]);
        }
        return true;
    }

    private void increaseCapacity(){
        capacity = capacity * 3 / 2 + 1;
        int[] newArray = new int[capacity];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        array = newArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AList1 aList1 = (AList1) o;
        return capacity == aList1.capacity && size == aList1.size && Arrays.equals(array, aList1.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(capacity, size);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }
}