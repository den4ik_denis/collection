package com.collection.aListTestData;

import com.collection.implementations.AList1;

public class AList1TestData {
    public static final int[] originalList1 = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public static final int[] originalList2 = new int[] {13, 34, 21, 0, 0, 0, 0, 0, 0, 0};

    public static final int[] originalList2ForAList = new int[] {13, 34, 21};

    public static final AList1 originalAList1 = new AList1();

    public static final AList1 originalAList2 = new AList1( originalList2ForAList);

    public static final int[] addList1 = new int[] {15};
    public static final int[] addList2 = new int[] {13, 34, 21, 20};

    public static final AList1 addAList1 = new AList1(addList1);

    public static final AList1 addAList2 = new AList1(addList2);

    public static final int[] addByIndexList1 = new int[] {10};
    public static final int[] addByIndexList2 = new int[] {13, 34, 25, 21};

    public static final AList1 addByIndexAList1 = new AList1(addByIndexList1);

    public static final AList1 addByIndexAList2 = new AList1(addByIndexList2);

    public static final int[] removeList1 = new int[] {34, 21};

    public static final int[] removeList2 = new int[] {13, 34};

    public static final AList1 removeAList1 = new AList1(removeList1);

    public static final AList1 removeAList2 = new AList1(removeList2);

    public static final int[] removeByIndexList1 = new int[] {34, 21};

    public static final int[] removeByIndexList2 = new int[] {13, 34};

    public static final AList1 removeByIndexAList1 = new AList1(removeByIndexList1 );

    public static final AList1 removeByIndexAList2 = new AList1(removeByIndexList2);

    public static final int[] setList1 = new int[] {13, 22, 21};

    public static final int[] setList2 = new int[] {13, 34, 11};

    public static final AList1 setAList1 = new AList1(setList1);

    public static final AList1 setAList2 = new AList1(setList2);

    public static final int[] removeAllList1 = new int[] {34};

    public static final int[] removeAllList2 = new int[] {};

    public static final AList1 removeAllAList1 = new AList1(removeAllList1);

    public static final AList1 removeAllAList2 = new AList1(removeAllList2);


}
