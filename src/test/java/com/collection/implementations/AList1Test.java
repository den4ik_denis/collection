package com.collection.implementations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Field;
import java.util.Arrays;

import static com.collection.aListTestData.AList1TestData.*;

public class AList1Test {
    AList1 cut = new AList1();

    static Arguments[] clearTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, originalList1, 0, originalAList1),
                Arguments.arguments(10, originalList2, 3, originalAList1)
        };
    }

    @ParameterizedTest
    @MethodSource("clearTestArgs")
    void clearTest(int reflectC, int[] reflectA, int reflectS, AList1 expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.clear();

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] getTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, originalList2, 3, 0, 13),
                Arguments.arguments(10, originalList2, 3, 2, 21)
        };
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(int reflectC, int[] reflectA, int reflectS, int index, int expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        int actual = cut.get(index);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] addTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList1, originalList1.length), 0, 15, addAList1),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 20, addAList2)
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(int reflectC, int[] reflectA, int reflectS, int input, AList1 expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.add(input);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] addByIndexTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList1, originalList1.length), 0, 0, 10, addByIndexAList1),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 2, 25, addByIndexAList2)
        };
    }

    @ParameterizedTest
    @MethodSource("addByIndexTestArgs")
    void addByIndexTest(int reflectC, int[] reflectA, int reflectS, int index, int input, AList1 expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.add(index, input);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 13, removeAList1),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 21, removeAList2)
        };
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int reflectC, int[] reflectA, int reflectS, int number, AList1 expected)
            throws NoSuchFieldException, IllegalAccessException{
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.remove(number);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeByIndexTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 0, removeByIndexAList1),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 2, removeByIndexAList2)
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int reflectC, int[] reflectA, int reflectS, int index,  AList1 expected)
            throws NoSuchFieldException, IllegalAccessException{
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.removeByIndex(index);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] containsTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 15, false),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 34, true)
        };
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(int reflectC, int[] reflectA, int reflectS, int number, boolean expected)
            throws NoSuchFieldException, IllegalAccessException{
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        boolean actual = cut.contains(number);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] setTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 1, 22, setAList1),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3, 2, 11, setAList2)
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int reflectC, int[] reflectA, int reflectS, int index, int number, AList1 expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.set(index, number);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] toArrayTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList1, originalList1.length), 0,
                        new int[] {}),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3,
                        new int[] {13, 34, 21})
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(int reflectC, int[] reflectA, int reflectS, int[] expected)
            throws IllegalAccessException, NoSuchFieldException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        int[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] removeAllTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3,
                        new int[] {13, 54, 12, 21, 69}, removeAllAList1),
                Arguments.arguments(10, Arrays.copyOf(originalList2, originalList2.length), 3,
                        new int[] {98, 13, 34, 21, 69}, removeAllAList2)
        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(int reflectC, int[] reflectA, int reflectS, int[] input, AList1 expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.removeAll(input);

        Assertions.assertEquals(expected, cut);
    }
}
